<h2 style="text-transform: uppercase; text-align: center"><span>Tài liệu sử dụng LazySizes Tiếng Việt</span></h2>

><em>Tài liệu được tìm hiểu và biên soạn bởi Nguyễn Văn Anh</em>

*Lần đầu tiên viết tài liệu nên còn nhiều chỗ lủng củng.... Trong quá trình học tập và làm việc sẽ không ngừng trau dồi và hoàn thiện hơn ...*

***
**Lazy Loading Là Gì?**
```
Lazy Loading(Lười tải, Tải chậm) hiểu nôm na thì nó là việc load dữ liệu khi cần sử dụng đến chúng.Chẳng hạn nhiều người dùng khi vào 1 page còn chẳng kéo xuống hết đến cuối trang để xem toàn bộ nội dung thì ta cần gì load toàn bộ nội dung trước?Việc ta nên làm là người dùng scroll đến đâu ta sẽ load dữ liệu đến đấy.Lazy Loading có thể áp dụng cho bất cứ resource nào trên 1 page,thậm chí là file JavaScript . Giờ thì chúng ta tập trung vào việc Lazy Loanding Images(load images khi thật sự cần).
```
**Tại sao nên dùng Lazy Loading?**
```
Lazy loading giảm việc tải dữ liệu => Tốc độc load trang nhanh hơn và giảm chí phí (bằng cách giảm tổng số bytes transferred)
```
<h3>1. Cách thức cài đặt.</h3>

>Tìm hiểu chi tiết thư viện tại git: https://github.com/aFarkas/lazysizes

Để bắt đầu sử dụng lazysizes, ta cần tải tệp Javascript vào website của chúng ta
Cài đặt bằng cdnjs: 
```html
<script src="https://unpkg.com/lazysizes@5.2.0/lazysizes.js"></script>
```
Bằng cách khác, có thể cài đặt qua npm hoặc bower : 
``` 
$ npm install lazysizes --save
```

```
$ bower install lazysize --save
```
Sau khi tải xuống bộ thư viện lazysizes, ta tiến hành import chúng vào trang web thông qua dòng lệnh HTML :
```html
<script src="[PATH FILE: lazysizes.min.js]"></script>
```

>Đối với nội bộ tài liệu về. Sau khi clone về thiết bị. Tiến hành chạy `npm install lazysizes --save` tại thư mục gốc của tài liệu.
<h3>2.Các chức năng và cách thức sử dụng</h3>
Sau đây là các mô tả về chức năng và cách hức sử dụng chúng trong website. 

>Thêm vào tất cả các ``img`` và ``iframe`` class: ``lazyload`` . Thay thế tên  ``src`` thành ``data-src`` | ``srcet`` thành ``data-srcet`` .

Ví dụ mô tả như sau: 
```html
<!-- Hình ảnh bình thường -->
<img src="image.png" class="class-images">
<!-- Hình ảnh sau khi được thiết lập LazySizes -->
<img data-src="image.png" class="class-images lazyload">
```
Một tài liệu mẫu (Trang HTML) cơ bản được thực hiện như sau :
``` html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Lazyload without preload</title>
    <style type="text/css">
      img {
        max-width: 100%;
      }
    </style>
  </head>

  <body>
    <img class='lazyload' data-src='http://file.hstatic.net/1000337768/file/test-image-1.jpg' />
    <img class='lazyload' data-src='http://file.hstatic.net/1000337768/file/test-image-2.jpg' />
    <img class='lazyload' data-src='http://file.hstatic.net/1000337768/file/test-image-3.jpg' />
    <img class='lazyload' data-src='http://file.hstatic.net/1000337768/file/test-image-4.jpg' />
    <img class='lazyload' data-src='http://file.hstatic.net/1000337768/file/test-image-5.jpg' />
    <img class='lazyload' data-src='http://file.hstatic.net/1000337768/file/test-image-6.jpg' />
    <img class='lazyload' data-src='http://file.hstatic.net/1000337768/file/test-image-7.jpg' />
    <img class='lazyload' data-src='http://file.hstatic.net/1000337768/file/test-image-8.jpg' />
    <img class='lazyload' data-src='http://file.hstatic.net/1000337768/file/test-image-9.jpg' />
    <img class='lazyload' data-src='http://file.hstatic.net/1000337768/file/test-image-10.jpg' />
    <img class='lazyload' data-src='http://file.hstatic.net/1000337768/file/test-image-11.jpg' />
    <img class='lazyload' data-src='http://file.hstatic.net/1000337768/file/test-image-12.jpg' />
    <img class='lazyload' data-src='http://file.hstatic.net/1000337768/file/test-image-13.jpg' />
    <img class='lazyload' data-src='http://file.hstatic.net/1000337768/file/test-image-14.jpg' />
    <img class='lazyload' data-src='http://file.hstatic.net/1000337768/file/test-image-15.jpg' />
    <img class='lazyload' data-src='http://file.hstatic.net/1000337768/file/test-image-16.jpg' />
    <img class='lazyload' data-src='http://file.hstatic.net/1000337768/file/test-image-17.jpg' />
    <img class='lazyload' data-src='http://file.hstatic.net/1000337768/file/test-image-18.jpg' />
    <img class='lazyload' data-src='http://file.hstatic.net/1000337768/file/test-image-19.jpg' />
    <img class='lazyload' data-src='http://file.hstatic.net/1000337768/file/test-image-20.jpg' />
    <img class='lazyload' data-src='http://file.hstatic.net/1000337768/file/test-image-21.jpg' />
    <img class='lazyload' data-src='http://file.hstatic.net/1000337768/file/test-image-22.jpg' />
    <img class='lazyload' data-src='http://file.hstatic.net/1000337768/file/test-image-23.jpg' />


   <script type="text/javascript" src='./node_modules/lazysizes/lazysizes.min.js'></script>
  </body>
</html>
```

Video mô tả về sự hoạt động cho đoạn mã trên:  [Xem video mô tả tại đây](https://gitlab.com/nguyenvananh.projects/documents/lazysizes.basedocument/blob/master/files/video-demo-1.mp4).
***
<h4>Thay đổi hình ảnh theo kích cỡ của website </h4> 

>*responsive example with automatic sizes calculation*

Cách sử dụng: Thêm thuộc tính `data-sizes="auto"` vào `img` nhé.
Ví dụ mô tả: 

```html
<!-- responsive example with automatic sizes calculation: -->
<img data-sizes="auto" data-src="https://farm9.staticflickr.com/8200/8248153196_7a7664e147_b.jpg" class="lazyload" />
```
*Mô tả:* Dựa vào kích thước của màn hình, sẽ thực hiện điều chỉnh kích thước của hình ảnh sao cho phù hợp với màn hình. Có thể kiểm tra thông qua thuộc tính "`sizes`" trong thẻ `img`

Một tài liệu mẫu (Trang HTML) cơ bản được thực hiện như sau :

``` html
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Lazyload without preload</title>
        <style type="text/css">
            img {
                max-width: 100%;
            }
        </style>
    </head>
    <body>  
        <img data-sizes="auto" data-src="https://farm9.staticflickr.com/8200/8248153196_7a7664e147_b.jpg" class="lazyload" />
        <script type="text/javascript" src='./node_modules/lazysizes/lazysizes.min.js'></script>
    </body>
</html>
```

Video mô tả về sự hoạt động cho đoạn mã trên:  [Xem video mô tả tại đây](https://gitlab.com/nguyenvananh.projects/documents/lazysizes.basedocument/blob/master/files/video-demo-2.mp4).
***Note:*** 
>Min Sizes = 964px.
>Thông thường nên có thuộc tính Css này: 
```CSS
img {
    width: 100%;
}
/* Đối với ảnh response */
img[data-sizes="auto"] {
     display: block; 
     width: 100%; 
}
```

***
<h4>Cách kết hợp data-src và data-srcset</h4>

>Thay vì hình ảnh sẽ zoom lớn nhỏ thông qua thuộc tính `sizes` thì giờ đây chúng ta có thể kết hợp nó với `data-srcset`. Mục đích của việc dùng `data-srcset` là: Dựa vào kusch cỡ của màn hình và đưa ra bức ảnh phù hợp nhất. VD ta có 3 bức ảnh: 300x300 px, 600x600px và 900x900 px. Đối với những màn hình 300px thì nó sẽ ưu tiên hiển thị bức ảnh 300x300, và tương tự với những hình ảnh lớn hơn.

```html
<!-- ví dụ phản hồi: --> 
<img 
    data-size="auto"
    data-src="image3.jpg"
    data-srcset="image3.jpg 600w, 
        image1.jpg 220w, 
        image2.jpg 300w, 
        image3. jpg 600w, 
        image4.jpg 900w" class="lazyload">
```

Một ví dụ mẫu cho một trang HTML :
```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Lazyload without preload</title>
  </head>
  <style>
    img[data-sizes="auto"] {
        display: block; 
        width: 100%; 
    }
  </style>
  <body>
    <!-- ví dụ phản hồi: --> 
  <img 
  data-size="auto"
  data-src="http://placehold.it/220x220"
  data-srcset="http://placehold.it/600x600 600w, 
  http://placehold.it/220x220 220w, 
  http://placehold.it/300x300 300w, 
  http://placehold.it/900x900 900w" class="lazyload">
   <script type="text/javascript" src='./node_modules/lazysizes/lazysizes.min.js'></script>
  </body>
</html>
```
Video mô tả về sự hoạt động cho đoạn mã trên:  [Xem video mô tả tại đây](https://gitlab.com/nguyenvananh.projects/documents/lazysizes.basedocument/blob/master/files/video-demo-3.mp4).

***
<h4>Lazyloading và Lazyloaded</h4>

>Lazysizes sử dụng class `lazyloading` trong khi ảnh đang được tải và khi hình ảnh tải xong thì class sẽ trở về giá trị `lazyloaded`. Ta có thể lợi dụng điều này để thêm hiệu ứng xuất hiện cho hình ảnh.

```css
/* Làm mờ ảnh sau khi tải */
.lazyload,
.lazyloading {
	opacity: 0;
}
.lazyloaded {
	opacity: 1;
	transition: opacity 300ms;
}
```

```css
/* Làm mờ hình ảnh trong khi tải và hiển thị một hình ảnh "loading" làm hình nền */

.lazyload {
	opacity: 0;
}

.lazyloading {
	opacity: 1;
	transition: opacity 300ms;
	background: #f7f7f7 url(loader.gif) no-repeat center;
}
```
***
<h4>Đối với những hình ảnh bị lỗi</h4>

>Đối với những hình ảnh bị lỗi, tức là trường hợp Load không thành công hình ảnh ( Không tồn tại thuộc tính `src` trong tập thuộc tính)

Có 2 cách giải quyết cho vấn đề này : 
```html
<!-- Thêm vào thuộc tính src với giá trị data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw== -->
src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
```
Cách thứ 2: Sử dụng Css để ẩn đi những hình ảnh không có thuộc tính src :
```css
img.lazyload:not([src]) {
	visibility: hidden;
}
```

***
<h2><center>SỬ DỤNG LAZYSIZES Ở MỘT LEVER KHÁC</center></h2>

<h3>Cấu hình bằng các lệnh Javascript</h3>

>Thực hiện cài đặt bằng cách khởi tạo một đối tượng cấu hình cục bộ có tên là `lazySizesConfig`
Lưu ý: Các cài đặt Javascript phải được thiết lập trước khi HTML được tải ( Tức là đặt trước các Code HTML)

```js
window.lazySizesConfig = window.lazySizesConfig || {};

// sử dụng class lazy thay vì class mặc định là lazyload
window.lazySizesConfig.lazyClass = 'lazy';

// sử dụng data-original thay vì mặc định là data-src
lazySizesConfig.srcAttr = 'data-original';

// page is optimized for fast onload event
lazySizesConfig.loadMode = 1;
```
Và sau đây là danh sách các thuộc tính có thể cài đặt và cấu hình
```html
lazySizesConfig.lazyClass (default: "lazyload"): Marker class for all elements which should be lazy loaded (There can be only one class. In case you need to add some other element, without the defined class, simply add it per JS: $('.lazy-others').addClass('lazyload');)
lazySizesConfig.preloadAfterLoad (default: false): Whether lazysizes should load all elements after the window onload event. Note: lazySizes will then still download those not-in-view images inside of a lazy queue, so that other downloads after onload are not blocked.)
lazySizesConfig.preloadClass (default: "lazypreload"): Marker class for elements which should be lazy pre-loaded after onload. Those elements will be even preloaded, if the preloadAfterLoad option is set to false. Note: This class can be also dynamically set ($currentSlide.next().find('.lazyload').addClass('lazypreload');).
lazySizesConfig.loadingClass (default: "lazyloading"): This class will be added to img element as soon as image loading starts. Can be used to add unveil effects.
lazySizesConfig.loadedClass (default: "lazyloaded"): This class will be added to any element as soon as the image is loaded or the image comes into view. Can be used to add unveil effects or to apply styles.
lazySizesConfig.expand (default: 370-500): The expand option expands the calculated visual viewport area in all directions, so that elements can be loaded before they become visible. The default value is calculated depending on the viewport size of the device. (Note: Reasonable values are between 300 and 1000 (depending on the expFactor option.) In case you have a lot of small images or you are using the LQIP pattern you can lower the value, in case you have larger images set it to a higher value. Also note, that lazySizes will dynamically shrink this value to 0 if the browser is currently downloading and expand it if the browser network is currently idling and the user not scrolling (by multiplying the expand option with 1.5 (expFactor)). This option can also be overridden with the [data-expand] attribute.
lazySizesConfig.minSize (default: 40): For data-sizes="auto" feature. The minimum size of an image that is used to calculate the sizes attribute. In case it is under minSize the script traverses up the DOM tree until it finds a parent that is over minSize.
lazySizesConfig.srcAttr (default: "data-src"): The attribute, which should be transformed to src.
lazySizesConfig.srcsetAttr (default: "data-srcset"): The attribute, which should be transformed to srcset.
lazySizesConfig.sizesAttr (default: "data-sizes"): The attribute, which should be transformed to sizes. Makes almost only makes sense with the value "auto". Otherwise, the sizes attribute should be used directly.
lazySizesConfig.customMedia (default: {}): The customMedia option object is an alias map for different media queries. It can be used to separate/centralize your multiple specific media queries implementation (layout) from the source[media] attribute (content/structure) by creating labeled media queries. (See also the custommedia extension).
lazySizesConfig.loadHidden (default: true): Whether to load visibility: hidden elements. Important: lazySizes will load hidden images always delayed. If you want them to be loaded as fast as possible you can use opacity: 0.001 but never visibility: hidden or opacity: 0.
lazySizesConfig.ricTimeout (default: 0): The timeout option used for the requestIdleCallback. Reasonable values between: 0, 100 - 1000. (Values below 50 disable the requestIdleCallback feature.)
lazySizesConfig.throttleDelay (default: 125): The timeout option used to throttle all listeners. Reasonable values between: 66 - 200.


<script>
window.lazySizesConfig = window.lazySizesConfig || {};
window.lazySizesConfig.customMedia = {
    '--small': '(max-width: 480px)',
    '--medium': '(max-width: 900px)',
    '--large': '(max-width: 1400px)',
};
</script>


<picture>
	<!--[if IE 9]><video style="display: none;><![endif]-->
	<source
		data-srcset="http://placehold.it/500x600/11e87f/fff"
		media="--small" />
	<source
		data-srcset="http://placehold.it/700x300"
		media="--medium" />
	<source
		data-srcset="http://placehold.it/1400x600/e8117f/fff"
		media="--large" />
	<source
        data-srcset="http://placehold.it/1800x900/117fe8/fff" />
    <!--[if IE 9]></video><![endif]-->
    <img

        data-src="http://placehold.it/1400x600/e8117f/fff"
        class="lazyload"
        alt="image with artdirection" />
</picture>

lazySizesConfig.expFactor (default: 1.5): The expFactor is used to calculate the "preload expand", by multiplying the normal expand with the expFactor which is used to preload assets while the browser is idling (no important network traffic and no scrolling). (Reasonable values are between 1.5 and 4 depending on the expand option).
lazySizesConfig.hFac (default: 0.8): The hFac (horizontal factor) modifies the horizontal expand by multiplying the expand value with the hFac value. Use case: In case of carousels there is often the wish to make the horizontal expand narrower than the normal vertical expand option. Reasonable values are between 0.4 - 1. In the unlikely case of a horizontal scrolling website also 1 - 1.5.
lazySizesConfig.loadMode (default: 2): The loadMode can be used to constrain the allowed loading mode. Possible values are 0 = don't load anything, 1 = only load visible elements, 2 = load also very near view elements (expand option) and 3 = load also not so near view elements (expand * expFactor option). This value is automatically set to 3 after onload. Change this value to 1 if you (also) optimize for the onload event or change it to 3 if your onload event is already heavily delayed.
lazySizesConfig.init (default: true): By default lazysizes initializes itself, to load in view assets as soon as possible. In the unlikely case you need to setup/configure something with a later script you can set this option to false and call lazySizes.init(); later explicitly.
```

<h4>Lazysizes cung cấp 3 sự kiện để dễ dàng control với hình ảnh:</h4>

* `lazybeforeunveil` : Sự kiện này sẽ được kích hoạt trên mỗi phần tử lazyload ngay trước khi chuyển đổi "tiết lộ". Sự kiện này có thể được sử dụng để mở rộng chức năng tiết lộ. 

* `lazyloaded`: Sau khi hình ảnh được tải đầy đủ, lazysizes gửi một lazyloadedsự kiện. Trong khi điều này thường lặp lại loadsự kiện bản địa, nó thường thuận tiện hơn để sử dụng.

* `lazybeforesizes`: Sự kiện này sẽ được kích hoạt trên mỗi phần tử với data-sizes="auto"thuộc tính ngay trước khi sizesthuộc tính được tính sẽ được đặt. Các event.detail.widthtài sản được thiết lập để chiều rộng tính của nguyên tố này và có thể được thay đổi với bất kỳ số. Trong trường hợp sự kiện này là defaultPreventedcác sizesthuộc tính sẽ không được thiết lập.